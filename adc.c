/***************************************************************************
* 
* Project           			        :  shakti devt board
* Name of the file	     		        :  adc.c
* Created date			                :  20.08.2019
* Name of Author               		    :  Kotte Sir,Soutrick Roy Chowdhury
* Email ID                       	    :  kottee1@gmail.com,soutrick97@gmail.com
*
*****************************************************************************/


#include "gpio_i2c.h"


#define PCF8591_SLAVE_ADDRESS 0x90 // I2C bus address //doubt
#define PCF8591_CONTROL_REGISTER 0x04
unsigned long delay = 100;
char readbuf[100];

void main()
{

     printf("START\n");
   /* using autoincreament flag register */

    I2cInit();
    I2cSendSlaveAddress(PCF8591_SLAVE_ADDRESS, I2C_WRITE, delay);//sending slave address
    printf("\n slave address sent");
    I2cWriteData(PCF8591_CONTROL_REGISTER, delay);//selecting auto increment in control register
    I2cStop(delay);
    printf(" \n auto increament bit of control register is set ");
    while(1)
    {
		printf("\n entering into shakti_readbytes");
		I2cSendSlaveAddress(PCF8591_SLAVE_ADDRESS, I2C_READ, delay);//sending slave address
		printf("\n Reading the adc values.");
		I2c_shakti_readbytes(readbuf,99,1,delay);
		printf("\n zero adc value : = %02x",readbuf[0]);
		printf("\n first adc value : = %02x",readbuf[1]);
		printf("\n second adc value: = %02x",readbuf[2]);
		printf("\n third adc value: = %02x",readbuf[3]);
		printf("\n fourth adc value: = %02x",readbuf[4]);
		printf("\n Read with the adc values.");
		DelayLoop(2000,1000);
    }
}

